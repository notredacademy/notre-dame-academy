Notre Dame Academy is San Diego’s preeminent choice for a progressive and comprehensive, first-class education for students from Preschool through 8th grade. We combine rigorous and advanced learning with an immersive Catholic community.

Address: 4345 Del Mar Trails Rd, San Diego, CA 92130, USA

Phone: 858-509-2300

Website: https://ndasd.org
